# 6. Computer controlled machining

This week I worked on designing 3d objects and using the CNC machine.

## Designing an ergonomic chair.

I decided to design an ergonomic chair which I saw once and I took a photo:

![](../images/week06/silla_ergonómica.jpg)

First I made a draw to take the measures and start to design:

![](../images/week06/boceto_silla.jpg)

Then I started to design it using Fusion 360:

![](../images/week06/diseño_fusion_silla.gif)

Before sending to cut it in the CNC machine, I had to make some probes about how to cut and join the parts. I made the next probes:

![](../images/week06/prueba_corte1.jpg)

![](../images/week06/prueba_corte2.jpg)

![](../images/week06/prueba_corte3.jpg)

![](../images/week06/prueba_corte4.jpg)

After making this probes, I finally send to cut the chair. Here are the results:

![](../images/week06/lista_corte_silla.jpg)

![](../images/week06/silla_terminada.jpg)

![](../images/week06/silla_armada.jpg)

![](../images/week06/silla_terminada2.jpg)



