# 2. Project management

## STEP 1 – GIT INSTALLATION
To install GIT we simply go to its official page and download the latest version:

<center>

  ![](../images/week02/image01.jpg)
</center>

Then we run the installer and give NEXT with the default options to all the windows that appear until the installation is finished:

<center>

  ![](../images/week02/image02.jpg)
</center>
<center>

  ![](../images/week02/image03.jpg)
</center>
<center>

  ![](../images/week02/image04.jpg)
</center>
<center>

  ![](../images/week02/image05.jpg)
</center>
<center>

  ![](../images/week02/image06.jpg)
</center>

## STEP 2 – CLONING THE PROJECT

First we are going to create a folder on our PC where the entire project will be stored:
<center>

  ![](../images/week02/paso2-01.jpg)
</center>

Inside this folder we are going to execute Git Bush:
<center>

  ![](../images/week02/paso2-02.jpg)
</center>

Now we have to config Git Bash indicating our user name and user email:
<center>

  ![](../images/week02/paso2-03.jpg)
</center>

Then we have to clone the repository from GitLab to my PC:
<center>

  ![](../images/week02/paso2-04.jpg)
</center>

Now my project has been created
<center>

![](../images/week02/paso2-05.jpg)
</center>

Next I have to create my password and put it in my GitLab:
<center>
  
![](../images/week02/paso2-06.jpg)
![](../images/week02/paso2-07.jpg)
![](../images/week02/paso2-08.jpg)
</center>

## STEP 3 – PUSHING MY PROJECT TO GITLAB

The next step is to edit the files in my PC and then push it to GitLab. We make it with the next commands:

git status
git add .
git commit -m "leyenda"
git push
<center>

![](../images/week02/paso3-01.jpg)
</center>

Now we can see all the changes in our GitLab page:
<center>

![](../images/week02/paso3-02.jpg)
![](../images/week02/paso3-03.jpg)
</center>