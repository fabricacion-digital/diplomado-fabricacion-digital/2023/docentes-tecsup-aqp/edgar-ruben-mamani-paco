#3. Computer Aided design
This week I worked on defining my final project idea and started to getting used to the documentation process.

##STEP 1: INSTALLING SOFTWARE
First we had to install software for making 2D and 3D design. I used the next softwares:

1. CorelDraw

![](../images/week03/3imagen01.jpg)

2. Fusión360

![](../images/week03/3imagen02.jpg)


Alternative we can use this others softwares:

- Inkscape (free)

![](../images/week03/3imagen03.jpg)

- Thinkercad

![](../images/week03/3imagen04.jpg)

##STEP 2: DESIGNING IN 2D SOFTWARE
I studied graphic design many years ago, so I decides to use CorelDraw because I dominate this software.
I made the Tecsup Logo:

![](../images/week03/3imagen05.jpg)

Then I send it to CutStudio. The image is here:

![](../images/week03/3imagen06.jpg)

I also used INKSCAPE to make the same design:

![](../images/week03/tecsup_inkscape.jpg)

MY CONCLUSIONS:
Inkscape is a free software to make designs, but COREL DRAW is much better because it has more tools to make better designs.

I made a design to use it in a educational module. The design is here:

![](../images/week03/3imagen07.jpg)


##STEP 3: DESIGNING IN 3D SOFTWARE

I cloned a mechanical piece in Fusion 360:

![](../images/week03/diseño-fusion.gif)


I designed a piece to support a lamp in a printed circuit board:

![](../images/week03/diseño-soporte.jpg) 


I also used TINKERCAD to make other kind of designs, here are the evidences:


![](../images/week03/diseño-tinkercad.jpg) 

MY CONCLUSIONS:
Tinkercad is much easier to use but Fusion 360 is really a good tool to create any kind of designs in a professional way.
