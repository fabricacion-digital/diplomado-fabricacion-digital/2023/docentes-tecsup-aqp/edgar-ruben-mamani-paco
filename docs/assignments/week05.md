# 5. 3D Scanning and Printing

This week I experimented doing some king of scanning and also making various printings.

I started scanning a little toy, but the results were not the better. Here are the evidences:

![](../images/week05/imagen01.jpg)

![](../images/week05/imagen02.jpg)

![](../images/week05/imagen03.jpg)

Then I decided to scan a bigger object, but this time I used an app in my phone. There are a lot of apps to make scanning.
After scanning the object, I printed it, but the results were not the better. Here are the evidences:

![](../images/week05/imagen04.jpg)

![](../images/week05/imagen06.jpg)

![](../images/week05/imagen07.jpg)

I also made some prints of diferente objects that I needed in my office. I downloaded them from Thingiverse:

![](../images/week05/imagen09.jpg)

![](../images/week05/imagen10.jpg)

Finally I decided to design an object I needed to duplicate. This was an special piece from a Machinery. Then I printed it:

![](../images/week05/3imagen09.jpg)

![](../images/week05/diseño-soporte.jpg)


