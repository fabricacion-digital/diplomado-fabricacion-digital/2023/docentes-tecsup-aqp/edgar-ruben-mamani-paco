# 4. Computer controlled cutting

This week I worked on using the laser cutting machine and the vinil cutter. I made some designs on my own and others I downloaded from internet. I´ll show you my results.

## STEP 1: DESIGNS FOR CUTTTING MACHINE AND RESULTS

I designed a support for my laptop. First I designed the structure which was very simple because it was a rectangle. Then I cut it in the laser cutter and finally I folded it properly using a heat resistance. 

The next step was to design the Tecsup logo in CorelDraw. Then I cutted it using the vinil cutter and finally I glued the vinyl over the acrylic. The result is as follows:

![](../images/week04/imagen01.jpg)

![](../images/week04/imagen02.jpg)

![](../images/week04/imagen03.jpg)

![](../images/week04/imagen03b.jpg) 

## STEP 2: DESIGNS FOR LASER MACHINE AND RESULTS

I designed a thermal camera... this is an educational module to use in my area. First I made the design using Corel Draw:

![](../images/week04/imagen04a.jpg) 

Then I made the cut using laser machine:

![](../images/week04/imagen04.jpg) 

and I installed it:

![](../images/week04/imagen05.jpg) 

![](../images/week04/imagen06.jpg) 

![](../images/week04/imagen07.jpg) 