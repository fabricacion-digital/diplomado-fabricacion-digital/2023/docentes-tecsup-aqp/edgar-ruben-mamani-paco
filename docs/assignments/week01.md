# 1. Principles and practices

This week I worked on defining my final project idea and started to getting used to the documentation process.

## My Research

Children who suffer from autism and mental health issues deserve a compassionate approach and specialized attention so they can develop and reach their full potential. Autism is a neurodevelopmental disorder that affects communication, social skills, and behavior, while mental health problems can encompass a wide range of conditions that affect children's mental well-being.

At a global level, there has been an increase in awareness and understanding of autism and mental health issues in recent years. International organizations and governments have implemented policies to promote inclusion and access to appropriate care and treatment. Efforts have also been made to reduce the stigma associated with these conditions to foster a more inclusive and tolerant society.

In Latin America, the focus on autism and mental health has been challenging due to various factors, such as limited financial resources and a gap in the availability of mental health services compared to more developed regions. However, significant progress has also been made. Several Latin American countries have implemented laws and policies to ensure educational and social inclusion for people with autism and other mental conditions.

Furthermore, various non-governmental organizations and civil society groups have worked tirelessly to raise awareness and provide support to affected families. Collaboration between healthcare professionals, educators, and communities has also been crucial in improving access to early detection, diagnosis, and appropriate treatment.

At a local level in Latin America, it is essential to advocate for increased investment in infrastructure and resources for the care and support of individuals with autism and mental health issues. Improving training and awareness among healthcare professionals and educators is necessary to ensure a more comprehensive and personalized approach. Specific policies and programs must also be established to address the needs of this vulnerable population.

Ultimately, the inclusion and support of children with autism and mental health issues should be a collective effort of society. We all have a responsibility to foster a culture of understanding, acceptance, and respect, providing these children with the opportunities they deserve to develop fully and lead meaningful lives.

## Purpose of the final project

I want to develop a wireless sensor that sticks to a person's hands to be able to send signals to a receiver and control the lighting and sound on a light panel. This will serve to stimulate the movement of the hands in a person.

<center>

![](../images/week01/mi_boceto.jpg)
</center>

 <center>	Prototype Idea</center>

## What will I do?

Firstly I am going to design the physical structure that will hold the sensors and on the other hand the physical structure for the receiver panel. Then I will design and manufacture the electronic cards that will make the wireless link between them.

## Who will use it?

This equipment is designed to be used by children with psychomotor difficulties, in such a way that the movement of their hands is stimulated. It could also adapt to the movement of the feet, the head, etc.