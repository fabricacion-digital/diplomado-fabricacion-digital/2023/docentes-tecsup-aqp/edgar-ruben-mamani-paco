# 7. Molding and Casting

This week I worked with molding

## Molding a Mario Bross toy

The first thing I had to do is to calculate de quantity of silicon to use:

![](../images/week07/1calculo_volumen.jpg)

Then I prepared an acrilyc cube with plastiline, to put there the toy:

![](../images/week07/molde_con_plastilina.jpg)

then I filled the rest of the cube with the silicon prepared:

![](../images/week07/llenado_con_silicona.jpg)

I had to wait 2 or 3 hours, then I put out the plastiline to fill de other side with silicon too:

![](../images/week07/llenado_ambos_lados.jpg)

After some hours, the molde was ready, so I filled it with resin to obtain the final piece:

![](../images/week07/sacando_molde.jpg)

![](../images/week07/desmoldando_pieza.jpg)

And the final product is here:

![](../images/week07/pieza_final.jpg)

I also designed a molde using 3d printer and TPU filament:

![](../images/week07/molde_TPU.jpg)


