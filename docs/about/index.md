# About me

![](../images/week01/mi_foto.jpg)

Hello, my name is Edgar Mamani, I have a bachelor's degree in Electronic Engineering and I have specialized studies in Microcontroller programming. I am currently a teacher in the Electricity and Electronics Area at the Tecsup Institute in Arequipa - Peru.

## My background

I was born in a nice city called Arequipa:

![](../images/week01/Ciudad_Arequipa.jpg)

## Previous work

Before becoming a teacher, I have worked at my own company called Labelin Electronics. This company specialized in designing and manufacturing electronic cards for various uses, electronic kits to assemble and custom electronic projects.

![](../images/week01/logo_labelin.png)

I will show you some of my projects I made:

![](../images/week01/proyecto1.jpg)
Sensor de Humedad inalámbrico por RF

![](../images/week01/proyecto2.jpg)
Tarjeta genérica con caja en impresión 3D

![](../images/week01/proyecto3.jpg)
Entrenador para microcontroladores PIC

![](../images/week01/proyecto4.jpg)
Mano impresa en 3D controlado por servomotor

![](../images/week01/proyecto5.jpg)
Amplificador para Sensores mioeléctricos

![](../images/week01/proyecto6.jpg)
Generador de Ozono para desinfección

![](../images/week01/proyecto7.jpg)
Microbot controlado por pulsadores orientado a niños

![](../images/week01/proyecto8.jpg)
Módulo robot basado en Arduino para aprendizaje

![](../images/week01/proyecto9.jpg)
Reloj digital gigante sincronizado por GPS








